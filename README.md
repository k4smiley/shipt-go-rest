# Go Rest Server 

A rest api for a product item, written in go and backed by AWS DynamoDB. 

## Getting Started  

### Prerequisites 

Please have go and a local instance of AWS DynamoDB installed. See https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DynamoDBLocal.html for instructions setting up AWS DynamoDB on your local machine. Also, please install aws-cli. 

### Installing 

Install Go dependencies: 

```
go get -u github.com/gorilla/mux
go get -u github.com/aws/aws-sdk-go/...
go get -u github.com/stretchr/testify/assert
go get -u github.com/google/uuid
```

If you havent, configure aws. You AWS Access Key ID and AWS Secret Access Key can be anything for local development. 

```
aws config 
```

Start your local instance of DynamoDB. By default, it runs on http://localhost:8000. If you've configured it diffrently, update your SHIPT_DYNAMODB_URL enviroment variable to the correct value. 

Create the tables needed for development. Use the corret endpoint if you are not using local DynamoDB. 

```
aws dynamodb create-table --cli-input-json file://create-table-shipt-products.json --endpoint-url http://localhost:8000
aws dynamodb create-table --cli-input-json file://create-table-shipt-products-test.json --endpoint-url http://localhost:8000
```

## Running the tests 

This project uses end to end tests. The tests always assume you are running DynamoDB on http://locahost:8000 and have created the test table as instructed above. To run the tests run

```
go test
```

## Building 

Once dependencies are installed (see Installing), build with 

```
go build
```

## Running 

Verify that DynamoDB is running on http://localhost:8000 or update your SHIPT_DYNAMODB_URL enviroment variable. Run the artifact. The server will start on http://localhost:8080. 

You can get all products with 

```
curl http://localhost:8080/products
```

You can create a new product with 

```
curl http://localhost:8080/products -d '{ "name": "Beer", "price": 1000 }'
```

You can get a specific product with by Id: 

```
curl http://localhost:8080/products/8ea451cc-74ee-4feb-9f1b-02ab7cebde15
```

You can update a product with

```
http://localhost:8080/products/8ea451cc-74ee-4feb-9f1b-02ab7cebde15 -X PUT -d '{ "id": "8ea451cc-74ee-4feb-9f1b-02ab7cebde15", "name": "Fancy Beer", "price": 1500 }'
```

You can patch a product with

```
curl http://localhost:8080/products/8ea451cc-74ee-4feb-9f1b-02ab7cebde15 -X PATCH -d '{ "name": "Fancy IPA" }'
```

## Next Steps

There are a number of ways that the server could be improved. 

UUID generation is random and has a (vanishingly) small chance of being duplicated. We could use a UUID service or DynamoDB itself to ensure no UUID collision. 

The /products endpoint is costly and will not scale with a large number of products. Given more time, we could add required pagination. Also, the endpoint does an in memory sort by price. This will run into problems with a large number of products. Instead, we could keep a list of all products in price order in our database. That way, the cost of sorting is amortized across product creation. 

API tokens could be used to track usage and throttle abusive users. This could also be used to restrict product access to certain users. 

We have a suite of integration tests, but we could solifiy our testing suite by adding unit tests. I opted for integration tests over unit tests because I believe integration tests are are more useful and robust than unit tests. 

Error reporting and heath checks should be added to make sure the server is running once deployed. 

Additional REST functionality: HEAD is not implemented. The server only supports json content encoding and does not return 501 not implemented for other content types. 

A load test suite would help with finding bottlenecks and improving performance. 
 
## License 

All rights reserved. Not for commercial use. 
