package handler

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"shipt-rest/storage"

	"github.com/gorilla/mux"
)

func ProductPutHandler(s storage.Storage) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		id := vars["id"]
		existing, err := s.GetProduct(id)
		notFound := storage.PersistedProduct{}
		if existing == notFound || existing.Deleted || err != nil {
			if err != nil {
				log.Print("Could not find product: "+id+": ", err)
			} else if existing == notFound {
				log.Print("Could not find product: ", id)
			} else {
				log.Print("Tried fo find deleted product: ", id)
			}
			unrecoverableError(w, http.StatusNotFound, err.Error())
			return
		}

		if err != nil {
			log.Print("Could not read body: ", err)
			unrecoverableError(w, http.StatusBadRequest, err.Error())
			return
		}

		body, err := ioutil.ReadAll(io.LimitReader(r.Body, max_body_size))
		if err != nil {
			log.Print("Could not read body: ", err)
			unrecoverableError(w, http.StatusBadRequest, err.Error())
			return
		}

		err = r.Body.Close()
		if err != nil {
			log.Print("Could not close body : ", err)
			unrecoverableError(w, http.StatusBadRequest, err.Error())
			return
		}

		var product Product
		err = json.Unmarshal(body, &product)
		if err != nil {
			log.Print("Could not unmarshal body: ", err)
			unrecoverableError(w, http.StatusBadRequest, err.Error())
			return
		}

		if product.Name == nil || product.Price == nil {
			log.Print("Missing fields. Body: ", string(body))
			unrecoverableError(w, http.StatusBadRequest, "name and price field are requred and cannot be empty or zero.")
			return
		}

		if *product.Id != id {
			msg := "Tried to update product id: " + string(body) + ", " + id
			log.Print(msg)
			unrecoverableError(w, http.StatusBadRequest, msg)
			return
		}

		err = s.SaveProduct(toPersistedProduct(product, false))
		if err != nil {
			unrecoverableError(w, http.StatusInternalServerError, "internal error")
			return
		}

		w.WriteHeader(http.StatusOK)
	}
}
