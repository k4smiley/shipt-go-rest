package handler

import (
	"encoding/json"
	"net/http"
	"shipt-rest/storage"
	"sort"
)

func ProductsHandler(s storage.Storage) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		persistedProducts, err := s.ScanProducts()
		if err != nil {
			unrecoverableError(w, http.StatusInternalServerError, "internal error")
			return
		}

		productList := []Product{}
		for _, value := range persistedProducts {
			if !value.Deleted {
				productList = append(productList, toProduct(value))
			}
		}

		/* in memory sort. if we get a lot of products, we could maintain a sorted list
		 * in dynamoDb and use that list to sort the products. we'd also want to implement
		 * paging -- possibly with "limit" and "offset" url parameters.
		 */
		sort.Slice(productList, func(i, j int) bool {
			return *productList[i].Price > *productList[j].Price
		})

		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(productList)
	}
}
