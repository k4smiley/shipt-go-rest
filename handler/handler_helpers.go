package handler

import (
	"encoding/json"
	"net/http"
	"shipt-rest/storage"
)

const max_body_size = 1048576 // one megabyte 

type Product struct {
	Id    *string `json: "id"`
	Name  *string `json: "name"`
	Price *int    `json: "price"`
}

type ErrorRsp struct {
	Status  int    `json: "status"`
	Message string `json: "message"`
}

func toPersistedProduct(p Product, deleted bool) storage.PersistedProduct {
	prod := storage.PersistedProduct{
		Name:    *p.Name,
		Price:   *p.Price,
		Deleted: deleted,
	}
	if p.Id != nil {
		prod.Id = *p.Id
	}
	return prod
}

func toProduct(p storage.PersistedProduct) Product {
	return Product{
		Id:    &p.Id,
		Name:  &p.Name,
		Price: &p.Price,
	}
}

func unrecoverableError(w http.ResponseWriter, statusCode int, message string) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(statusCode)
	resp := ErrorRsp{Status: statusCode, Message: message}
	_ = json.NewEncoder(w).Encode(resp)
}
