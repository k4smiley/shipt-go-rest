package handler

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"shipt-rest/storage"
)

func ProductPostHandler(s storage.Storage) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		body, err := ioutil.ReadAll(io.LimitReader(r.Body, max_body_size))
		if err != nil {
			log.Print("Could not read body: ", err)
			unrecoverableError(w, http.StatusBadRequest, err.Error())
			return
		}

		err = r.Body.Close()
		if err != nil {
			log.Print("Could not close body : ", err)
			unrecoverableError(w, http.StatusBadRequest, err.Error())
			return
		}

		var product Product
		err = json.Unmarshal(body, &product)
		if err != nil {
			log.Print("Could not unmarshal body: ", err)
			unrecoverableError(w, http.StatusBadRequest, err.Error())
			return
		}

		if product.Id != nil {
			log.Print("Not creating a product with specified Id")
			unrecoverableError(w, http.StatusBadRequest, "do not send an id when creating a product.")
			return

		}

		if product.Name == nil || product.Price == nil {
			log.Print("Missing fields. Body: ", string(body))
			unrecoverableError(w, http.StatusBadRequest, "name and price field are requred.")
			return
		}

		savedProd, err := s.CreateProduct(toPersistedProduct(product, false))
		if err != nil {
			log.Print("Could not persist product", err)
			unrecoverableError(w, http.StatusInternalServerError, "internal error")
			return
		}
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusCreated)
		err = json.NewEncoder(w).Encode(toProduct(savedProd))

		if err != nil {
			log.Print("Could not encode response: ", err)
			unrecoverableError(w, http.StatusInternalServerError, err.Error())
			return
		}
	}
}
