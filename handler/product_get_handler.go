package handler

import (
	"encoding/json"
	"log"
	"net/http"
	"shipt-rest/storage"

	"github.com/gorilla/mux"
)

func ProductGetHandler(s storage.Storage) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		id := vars["id"]
		existing, err := s.GetProduct(id)
		notFound := storage.PersistedProduct{}
		if existing == notFound || existing.Deleted || err != nil {
			if err != nil {
				log.Print("Could not find product: " + id + ": " + err.Error())
			} else if existing == notFound {
				log.Print("Could not find product: ", id)
			} else {
				log.Print("Tried fo find deleted product: ", id)
			}
			unrecoverableError(w, http.StatusNotFound, "no product with id: "+id)
			return
		}
		json.NewEncoder(w).Encode(toProduct(existing))
	}
}
