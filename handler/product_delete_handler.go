package handler

import (
	"net/http"
	"shipt-rest/storage"

	"github.com/gorilla/mux"
)

func ProductDeleteHandler(s storage.Storage) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		id := vars["id"]
		prod, err := s.GetProduct(id)
		if err != nil {
			unrecoverableError(w, http.StatusInternalServerError, "internal error")
			return
		}
		notFound := storage.PersistedProduct{}
		if prod == notFound {
			unrecoverableError(w, http.StatusNotFound, "no product with id: "+id)
			return
		}
		prod.Deleted = true
		s.SaveProduct(prod)
	}
}
