package main

import (
	"bytes"
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"shipt-rest/config"
	"shipt-rest/handler"
	"shipt-rest/routes"
	"shipt-rest/storage"
	"testing"
)

const max_body_size = 1048576

var s = storage.CreateStorage(config.Config{
	DynamoDBUrl:   "http://localhost:8000",
	ProductsTable: "Shipt-Products-Test",
})

func TestMain(m *testing.M) {
	clearTable(s)
	code := m.Run()
	os.Exit(code)
}

func TestSimpleHanlder(t *testing.T) {
	router := routes.InitRouter(s)

	// no products initall
	req, err := http.NewRequest("GET", "/products", nil)
	req.Header.Add("content-type", "application/json")
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)
	var products []handler.Product
	err = json.Unmarshal(rr.Body.Bytes(), &products)
	assert.Equal(t, []handler.Product{}, products)

	// test get 404s
	req, err = http.NewRequest("GET", "/products/1", nil)
	req.Header.Add("content-type", "application/json")
	rr = httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, http.StatusNotFound, rr.Code)

	// we can create some products
	celery := makeProduct("celery", 100)
	celJson, err := json.Marshal(&celery)
	if err != nil {
		t.Fatal(err)
	}

	req, err = http.NewRequest("POST", "/products", bytes.NewReader(celJson))
	req.Header.Add("content-type", "application/json")
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusCreated, rr.Code)
	var celResp handler.Product
	body, _ := ioutil.ReadAll(io.LimitReader(rr.Body, max_body_size))
	err = json.Unmarshal(body, &celResp)
	if err != nil {
		t.Errorf(err.Error())
	}
	celId := *celResp.Id
	expectedCel := makeProductId(celId, "celery", 100)
	assert.Equal(t, expectedCel, celResp)

	// we can create a product
	bread := makeProduct("bread", 99)
	breadJson, err := json.Marshal(&bread)
	if err != nil {
		t.Fatal(err)
	}

	req, err = http.NewRequest("POST", "/products", bytes.NewReader(breadJson))
	req.Header.Add("content-type", "application/json")
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusCreated, rr.Code)
	var breadResp handler.Product
	body, _ = ioutil.ReadAll(io.LimitReader(rr.Body, max_body_size))
	err = json.Unmarshal(body, &breadResp)
	if err != nil {
		t.Errorf(err.Error())
	}
	breadId := *breadResp.Id
	expectedBread := makeProductId(breadId, "bread", 99)
	assert.Equal(t, expectedBread, breadResp)

	// new products are listed on all page
	req, err = http.NewRequest("GET", "/products", nil)
	req.Header.Add("content-type", "application/json")
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)
	err = json.Unmarshal(rr.Body.Bytes(), &products)
	assert.Equal(t, []handler.Product{expectedCel, expectedBread}, products)

	// new products are listed on their id page
	req, err = http.NewRequest("GET", "/products/"+celId, nil)
	req.Header.Add("content-type", "application/json")
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusOK, rr.Code)
	err = json.Unmarshal(rr.Body.Bytes(), &celResp)
	assert.Equal(t, expectedCel, celResp)

	// we can put products
	freshCelery := makeProductId(celId, "fresh celery", 105)
	celJson, err = json.Marshal(&freshCelery)
	if err != nil {
		t.Fatal(err)
	}

	req, err = http.NewRequest("PUT", "/products/"+celId, bytes.NewReader(celJson))
	req.Header.Add("content-type", "application/json")
	if err != nil {
		t.Fatal(err)
	}
	rr = httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusOK, rr.Code)

	// puts are reflected on their id page
	req, err = http.NewRequest("GET", "/products/"+celId, nil)
	req.Header.Add("content-type", "application/json")
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusOK, rr.Code)
	expectedFreshCel := makeProductId(celId, "fresh celery", 105)
	var freshCelResp handler.Product
	err = json.Unmarshal(rr.Body.Bytes(), &freshCelResp)
	assert.Equal(t, expectedFreshCel, freshCelResp)

	// we can patch products
	veryFreshCelery := makeProduct("very fresh celery", 105)
	veryFreshCelery.Price = nil
	celJson, err = json.Marshal(&veryFreshCelery)
	if err != nil {
		t.Fatal(err)
	}

	req, err = http.NewRequest("PATCH", "/products/"+celId, bytes.NewReader(celJson))
	req.Header.Add("content-type", "application/json")
	if err != nil {
		t.Fatal(err)
	}
	rr = httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusOK, rr.Code)

	// patches are reflected on their id page
	req, err = http.NewRequest("GET", "/products/"+celId, nil)
	req.Header.Add("content-type", "application/json")
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusOK, rr.Code)
	expectedVeryFreshCel := makeProductId(celId, "very fresh celery", 105)
	var veryFreshCelResp handler.Product
	err = json.Unmarshal(rr.Body.Bytes(), &veryFreshCelResp)
	assert.Equal(t, expectedVeryFreshCel, veryFreshCelResp)

	// we can delete products
	req, err = http.NewRequest("DELETE", "/products/"+celId, nil)
	req.Header.Add("content-type", "application/json")
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)

	// deleted products are gone
	req, err = http.NewRequest("GET", "/products/"+celId, nil)
	req.Header.Add("content-type", "application/json")
	rr = httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, http.StatusNotFound, rr.Code)

}

func clearTable(s storage.Storage) {
	products, err := s.ScanProducts()
	if err != nil {
		panic("Could not get products. Is DymandoDB running? " + err.Error())
	}
	for _, prod := range products {
		err = s.DeleteProduct(prod.Id)
		if err != nil {
			panic("Could not delete product: " + err.Error())
		}
	}
}

func makeProductId(id string, name string, price int) handler.Product {
	return handler.Product{Id: &id, Name: &name, Price: &price}
}

func makeProduct(name string, price int) handler.Product {
	return handler.Product{Name: &name, Price: &price}
}
