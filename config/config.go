package config

type Config struct {
	DynamoDBUrl   string
	ProductsTable string
}
