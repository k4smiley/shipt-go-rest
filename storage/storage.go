package storage

import (
	"log"
	"shipt-rest/config"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/google/uuid"
)

type PersistedProduct struct {
	Id      string
	Name    string
	Price   int
	Deleted bool
}

type Storage interface {
	CreateProduct(p PersistedProduct) (PersistedProduct, error)
	SaveProduct(p PersistedProduct) error
	DeleteProduct(id string) error
	GetProduct(id string) (PersistedProduct, error)
	ScanProducts() ([]PersistedProduct, error)
}

type dynamodbConfig struct {
	Client        dynamodb.DynamoDB
	ProductsTable string
}

func CreateStorage(config config.Config) Storage {
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
		Config: aws.Config{
			Endpoint: &config.DynamoDBUrl,
		},
	}))
	svc := dynamodb.New(sess)
	return dynamodbConfig{Client: *svc, ProductsTable: config.ProductsTable}
}

func (db dynamodbConfig) CreateProduct(p PersistedProduct) (PersistedProduct, error) {
	/* Generate a random UUID for a new product. The id is large enough that there should be no collisions.
	 * In the future, we could use DyanmoDB or another service to ensure we get a unique id.
	 */
	id, err := uuid.NewRandom()
	if err != nil {
		return PersistedProduct{}, nil
	}
	p.Id = id.String()
	err = db.SaveProduct(p)
	if err != nil {
		return PersistedProduct{}, err
	}
	return p, nil
}

func (db dynamodbConfig) SaveProduct(p PersistedProduct) error {
	marshaled, err := dynamodbattribute.MarshalMap(p)
	if err != nil {
		log.Print("Could not marshal product: " + err.Error())
		return err
	}
	_, err = db.Client.PutItem(&dynamodb.PutItemInput{Item: marshaled, TableName: aws.String(db.ProductsTable)})
	if err != nil {
		log.Print("Error calling PutItem: " + err.Error())
		return err
	}
	return nil
}

func (db dynamodbConfig) GetProduct(id string) (PersistedProduct, error) {
	result, err := db.Client.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(db.ProductsTable),
		Key: map[string]*dynamodb.AttributeValue{
			"Id": {
				S: aws.String(id),
			},
		},
	})
	if err != nil {
		log.Print("Error calling GetItem: " + err.Error())
		return PersistedProduct{}, err
	}
	persistedProd := PersistedProduct{}

	err = dynamodbattribute.UnmarshalMap(result.Item, &persistedProd)
	if err != nil {
		log.Print("Error unmarshling value: " + err.Error())
		return PersistedProduct{}, nil
	}
	return persistedProd, nil
}

func (db dynamodbConfig) ScanProducts() ([]PersistedProduct, error) {
	params := &dynamodb.ScanInput{
		TableName: aws.String(db.ProductsTable),
	}
	result, err := db.Client.Scan(params)
	if err != nil {
		log.Print("Error scaning products: " + err.Error())
		return []PersistedProduct{}, err
	}

	items := []PersistedProduct{}

	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &items)
	if err != nil {
		log.Print("Error marshaling scanned products: " + err.Error())
		return []PersistedProduct{}, err
	}

	return items, nil
}

// warning: deletes from the database. to do a regular delete, call saveproduct with deleted = true
func (db dynamodbConfig) DeleteProduct(id string) error {
	input := &dynamodb.DeleteItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"Id": {
				S: aws.String(id),
			},
		},
		TableName: aws.String(db.ProductsTable),
	}

	_, err := db.Client.DeleteItem(input)
	if err != nil {
		log.Print("Error marshaling deleting products: " + err.Error())
		return err
	}
	return nil
}
