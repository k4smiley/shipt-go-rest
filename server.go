package main

import (
	"log"
	"net/http"
	"os"
	"shipt-rest/config"
	"shipt-rest/routes"
	"shipt-rest/storage"
)

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

func main() {
	config := config.Config{
		DynamoDBUrl:   getEnv("SHIPT_DYNAMODB_URL", "http://localhost:8000"),
		ProductsTable: getEnv("SHIPT_PRODUCTS_TABLE", "Shipt-Products"),
	}
	s := storage.CreateStorage(config)

	router := routes.InitRouter(s)
	log.Print("Starting server.")
	http.Handle("/", router)
	if err := http.ListenAndServe(":8080", nil); err != nil {
		panic(err)
	}
}
