package routes

import (
	"github.com/gorilla/mux"
	"net/http"
	"shipt-rest/handler"
	"shipt-rest/middleware"
	"shipt-rest/storage"
)

func InitRouter(s storage.Storage) *mux.Router {
	router := mux.NewRouter()
	for _, route := range routes(s) {
		router.
			Methods(route.Method).
			Path(route.Path).
			Handler(middleware.TimingLogger(route.Handler, route.Path))
	}
	return router
}

type route struct {
	Method  string
	Path    string
	Handler http.HandlerFunc
}

func routes(s storage.Storage) []route {
	return []route{
		route{"GET", "/products", handler.ProductsHandler(s)},
		route{"GET", "/products/{id}", handler.ProductGetHandler(s)},
		route{"PUT", "/products/{id}", handler.ProductPutHandler(s)},
		route{"PATCH", "/products/{id}", handler.ProductPatchHandler(s)},
		route{"POST", "/products", handler.ProductPostHandler(s)},
		route{"DELETE", "/products/{id}", handler.ProductDeleteHandler(s)},
	}
}
