package middleware

import (
	"log"
	"net/http"
	"time"
)

func TimingLogger(handler http.Handler, loggerName string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		startTime := time.Now()

		handler.ServeHTTP(w, r)

		log.Printf(
			"%s\t%s\t%s\t%s",
			r.Method,
			r.RequestURI,
			loggerName,
			time.Since(startTime))
	})
}
